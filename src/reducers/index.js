import { combineReducers } from "redux";

import ArticleReducer from "./Articles";

const rootReducer = combineReducers({
  articles: ArticleReducer,
});
export default rootReducer;
