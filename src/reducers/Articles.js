import {
  FETCH_TOPNEWS,
  FETCH_TOPNEWS_CATEGORIES,
  FETCH_TOPNEWS_SEARCH,
} from "../actions/actionTypes";

export default function (state = [], action) {
  switch (action.type) {
    case FETCH_TOPNEWS:
      return [action.payload.data, ...state];
    case FETCH_TOPNEWS_CATEGORIES:
      return [action.payload.data, ...state];
    case FETCH_TOPNEWS_SEARCH:
      return [action.payload.data, ...state];
  }
  return state;
}
