import React from "react";
import styled from "styled-components";

const CustomLoaderContainer = styled.div`
  width: 100%;
  text-align: center;
  margin: 50px 0;
`;

const CustomLoaderAnimation = styled.div`
    display: inline-block;
    border: 4px solid #f3f3f3;
    border-radius: 50%;
    border-top: 4px solid #1976D2;
    width: 20px;
    height: 20px;
    -webkit-animation: spin 2s linear infinite;
    -webkit-animation: spin 1s linear infinite;
    animation: spin 1s linear infinite;

  @keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`;

export default function CustomLoaderPartial() {
  return (
    <CustomLoaderContainer>
      <CustomLoaderAnimation />
    </CustomLoaderContainer>
  );
}
