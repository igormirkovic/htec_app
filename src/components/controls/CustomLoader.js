import React from "react";
import styled from "styled-components";

const CustomLoaderWrapper = styled.div`
  top: 0;
  left: 0;
  position: fixed;
  width: 100%;
  height: 100%;
  background: white;
  z-index: 100;
`;

const CustomLoaderContainer = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
`;

const CustomLoaderSpinner = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

const CustomLoaderAnimation = styled.div`
   border: 9px solid #f3f3f3;
   border-radius: 50%;
   border-top: 9px solid #1976D2;
   width: 60px;
   height: 60px;
   -webkit-animation: spin 2s linear infinite; /* Safari */
   animation: spin 1s linear infinite;

  @keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`;

export default function CustomLoader() {
  return (
    <CustomLoaderWrapper>
      <CustomLoaderContainer>
        <CustomLoaderSpinner>
          <CustomLoaderAnimation />
        </CustomLoaderSpinner>
      </CustomLoaderContainer>
    </CustomLoaderWrapper>
  );
}
