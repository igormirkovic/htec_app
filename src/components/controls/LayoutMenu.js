import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import styled from "styled-components";
import { Redirect, Link, useLocation } from "react-router-dom";
import PropTypes from "prop-types";
import { RegionContext } from "../../contexts/region";

const LayoutMenuContainer = styled.div`
  width: 100%;
  height: 50px;
  background: #1976d2;
  position: relative;
`;

const LayoutMenuTab = styled(Button)`
  && {
    color: white;
  }
  padding: 7px 14px;
  width: 100px;
  height: 100%;
  color: white;
  display: inline-block;
  text-align: center;
  line-height: 50px;

  &:hover {
    background-color: #5469d4;
  }
`;

export default function LayoutMenu({ children }) {
  const { setRegion } = RegionContext();
  const [getRedirect] = useState("");

  const location = useLocation();

  if (getRedirect) {
    return (
      <Redirect
        push
        to={{
          pathname: getRedirect,
        }}
      />
    );
  }

  return (
    <LayoutMenuContainer>
      <Link to="/topnews">
        <LayoutMenuTab>Top news</LayoutMenuTab>
      </Link>
      <Link to="/categories">
        <LayoutMenuTab>Categories</LayoutMenuTab>
      </Link>
      <Link to="/search">
        <LayoutMenuTab>Search</LayoutMenuTab>
      </Link>
      <div className="region_action">
        <Button
          disabled={location.pathname.indexOf("articles") > -1}
          onClick={() => setRegion("GB")}
        >
          GB
        </Button>
        <Button
          disabled={location.pathname.indexOf("articles") > -1}
          onClick={() => setRegion("DE")}
        >
          DE
        </Button>
      </div>
      <div>{children}</div>
    </LayoutMenuContainer>
  );
}

LayoutMenu.propTypes = {
  children: PropTypes.object,
};
