import React from "react";
import AliceCarousel from "react-alice-carousel";
import "react-alice-carousel/lib/alice-carousel.css";
import PropTypes from "prop-types";
import NewsCard from "../NewsCard";

export default function CustomSlider({ data }) {
  const renderData = () => {
    return data.map((article, index) => {
      return <NewsCard key={index + "_"} article={article} />;
    });
  };

  return (
    <AliceCarousel autoPlay autoPlayInterval="3000">
      {renderData()}
    </AliceCarousel>
  );
}

CustomSlider.propTypes = {
  data: PropTypes.array,
};
