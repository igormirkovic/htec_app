import React from "react";
import "react-alice-carousel/lib/alice-carousel.css";
import { TextField } from "@material-ui/core";
import PropTypes from "prop-types";

export default function CustomTextfield(props) {
  return (
    <TextField
      value={props.value}
      onChange={props.onChange}
      label={props.label}
      variant={props.variant}
    />
  );
}

CustomTextfield.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  label: PropTypes.string,
  variant: PropTypes.string,
};
