import React from "react";
import { Switch, Route } from "react-router-dom";
import TopNewsPage from "./pages/TopNewsPage";
import ArticlePage from "./pages/ArticlePage";
import CategoriesPage from "./pages/CategoriesPage";
import SearchPage from "./pages/SearchPage";
import LayoutMenu from "./controls/LayoutMenu";

import "../assets/style/style.scss";

const App = () => {
  return (
    <>
      <LayoutMenu>
        <Switch>
          <Route exact path="/topnews" component={TopNewsPage} />
          <Route exact path="/categories" component={CategoriesPage} />
          <Route exact path="/search" component={SearchPage} />
          <Route exact path="/articles/:id" component={ArticlePage} />
        </Switch>
      </LayoutMenu>
    </>
  );
};
export default App;
