import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { RegionContext } from "../contexts/region";
import CustomLoaderPartial from "./controls/CustomLoaderPartial";
import CustomSlider from "./controls/CustomSlider";
import expandImg from "../assets/img/chevron-down-solid.svg";
import collapseImg from "../assets/img/chevron-up-solid.svg";
import { fetchTopNewsByCountryAndCategories } from "../actions";

const CategoryContainer = styled.div`
  width: 100%;
  margin: 0 auto;
  margin-bottom: 20px;
`;

const CategoryTitle = styled.div`
  text-align: left;
  font-size: 15px;
  font-weight: bolder;
  display: inline-block;
  vertical-align: middle;
  margin-right: 10px;
`;

const CategoryIcon = styled.div.attrs((props) => ({
  className: props.className,
}))`
  display: inline-block;
  vertical-align: middle;
  background-image: url(${expandImg});
  width: 10px;
  height: 10px;
  background-size: cover;
  cursor: pointer;

  &.collapsed {
    background-image: url(${collapseImg});
  }
`;

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchTopNewsByCountryAndCategories }, dispatch);
}

function Category({ category, fetchTopNewsByCountryAndCategories }) {
  const { getRegion } = RegionContext();

  const [getTopNews, setTopNews] = useState([]);
  const [getLoader, setLoader] = useState(true);
  const [isExpand, setExpand] = useState(true);

  useEffect(() => {
    fetchTopNewsByCountryAndCategories(getRegion, category.name).then((res) => {
      if (res.payload && res.payload.data) {
        setTopNews(res.payload.data.articles);
        setLoader(false);
      }
    });
  }, [getRegion]);

  if (getLoader) {
    return <CustomLoaderPartial />;
  }

  return (
    <CategoryContainer>
      <CategoryTitle>{category.name}</CategoryTitle>
      <CategoryIcon
        className={isExpand ? "collapsed" : ""}
        onClick={() => setExpand(!isExpand)}
      />

      {isExpand && <CustomSlider data={getTopNews} />}
    </CategoryContainer>
  );
}

export default connect(null, mapDispatchToProps)(Category);

Category.propTypes = {
  category: PropTypes.object,
  fetchTopNewsByCountryAndCategories: PropTypes.func,
};
