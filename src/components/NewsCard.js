import React, { useState } from "react";
import styled from "styled-components";
import { Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import { useTranslation } from "react-i18next";

const NewsCardContainer = styled.div`
  margin: 20px;
  width: 345px;
  border-radius: 3px;
  color: black;
  display: inline-block;
  box-shadow: 0px 2px 1px -1px rgba(0, 0, 0, 0.2),
    0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0, 0, 0, 0.12);
  border-radius: 4px;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
`;

const NewsCardTitle = styled.div`
  font-size: 15px;
  font-weight: 400;
  line-height: 1.334;
  letter-spacing: 0em;
  height: 60px;
`;

const NewsCardImage = styled.img`
  width: 100%;
  object-fit: cover;
  height: 140px;
`;

const NewsCardContent = styled.div`
  padding: 16px;
`;

const NewsCardMoreLabel = styled.div`
  width: 100%;
  text-align: right;
  cursor: pointer;
  user-select: none;
  padding: 16px;
  box-sizing: border-box;
`;

export default function NewsCard({ article }) {
  const [getRedirect, setRedirect] = useState("");
  const { t } = useTranslation();
  const [getCurrentArticle, setCurrentArticle] = useState({});

  if (getRedirect && getCurrentArticle) {
    return (
      <Redirect
        push
        to={{
          pathname: getRedirect,
          state: { article: getCurrentArticle },
        }}
      />
    );
  }

  const RedirectAndPassState = () => {
    setRedirect(`/articles/${article.publishedAt}`);
    setCurrentArticle(article);
  };

  return (
    <NewsCardContainer>
      <NewsCardImage src={article.urlToImage} />
      <NewsCardContent>
        <NewsCardTitle>{article.title}</NewsCardTitle>
      </NewsCardContent>
      <NewsCardMoreLabel>
        <Button onClick={RedirectAndPassState}>{t("news.more")}</Button>
      </NewsCardMoreLabel>
    </NewsCardContainer>
  );
}

NewsCard.propTypes = {
  article: PropTypes.object,
};
