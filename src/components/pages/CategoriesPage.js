import React from "react";
import Category from "../Category";
import styled from "styled-components";
import { RegionContext } from "../../contexts/region";
import { useTranslation } from "react-i18next";

const categories = [
  {
    name: "Business",
    url: "business",
  },
  {
    name: "Entertainment",
    url: "entertainment",
  },
  {
    name: "General",
    url: "general",
  },
  {
    name: "Health",
    url: "health",
  },
  {
    name: "Science",
    url: "science",
  },
  {
    name: "Sports",
    url: "sports",
  },
  {
    name: "Technology",
    url: "technology",
  },
];

const CategoryPageContainer = styled.div`
  padding: 30px 100px;
  box-sizing: border-box;
`;

const CategoryPageTitle = styled.div`
  font-size: 18px;
  font-weight: bolder;
  text-align: left;
  width: 100%;
  margin-bottom: 20px;
`;

export default function CategoriesPage() {
  const { getRegion } = RegionContext();
  const { t } = useTranslation();

  const renderCategories = () => {
    return categories.map((category, index) => {
      return <Category key={index + "_"} category={category} />;
    });
  };

  return (
    <CategoryPageContainer>
      <CategoryPageTitle>
        {t("pages.categories")}
        {getRegion}:
      </CategoryPageTitle>
      {renderCategories()}
    </CategoryPageContainer>
  );
}
