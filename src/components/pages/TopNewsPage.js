import React, { useState, useEffect } from "react";
import styled from "styled-components";
import NewsCard from "./../NewsCard";
import PropTypes from "prop-types";

import { RegionContext } from "../../contexts/region";
import CustomLoader from "../controls/CustomLoader";
import { useTranslation } from "react-i18next";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { fetchTopNewsByCountry } from "../../actions";

const NewsCardWrapperContainer = styled.div`
  padding: 30px 100px;
  display: flex;
  flex-wrap: wrap;
`;

const NewsCardWrapperTitle = styled.div`
  font-size: 18px;
  font-weight: bolder;
  text-align: left;
  width: 100%;
  margin-bottom: 20px;
`;

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchTopNewsByCountry }, dispatch);
}

function TopNewsPage({ fetchTopNewsByCountry }) {
  const { getRegion } = RegionContext();
  const [getArticles, setArticles] = useState([]);
  const [getLoader, setLoader] = useState(true);
  const { t } = useTranslation();

  const renderNews = () => {
    return getArticles.map((article, index) => {
      return <NewsCard key={"_" + index} article={article} />;
    });
  };

  useEffect(() => {
    fetchTopNewsByCountry(getRegion).then((res) => {
      if (res.payload && res.payload.data) {
        setLoader(false);
        setArticles(res.payload.data.articles);
      }
    });
  }, [getRegion]);

  if (getLoader) {
    return <CustomLoader />;
  }

  return (
    <NewsCardWrapperContainer>
      <NewsCardWrapperTitle>
        {t("pages.topnews") + getRegion}{" "}
      </NewsCardWrapperTitle>
      {renderNews()}
    </NewsCardWrapperContainer>
  );
}

export default connect(null, mapDispatchToProps)(TopNewsPage);

TopNewsPage.propTypes = {
  fetchTopNewsByCountry: PropTypes.func,
};
