import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useTranslation } from "react-i18next";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";

import { RegionContext } from "../../contexts/region";
import { fetchTopNewsByCountryAndTerm } from "../../actions";
import CustomTextfield from "../controls/CustomTextfield";
import NewsCard from "../NewsCard";
import CustomLoaderPartial from "../controls/CustomLoaderPartial";

const SearchPageContainer = styled.div`
  width: 100%;
  height: 100%;
  text-align: center;
  padding: 30px 100px;
  box-sizing: border-box;
`;

const SearchPageTitle = styled.div`
  font-size: 18px;
  font-weight: bolder;
  text-align: left;
  width: 100%;
  margin-bottom: 20px;
`;

const SearchPageContent = styled.div`
  width: 100%;
  margin-top: 30px;
`;

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchTopNewsByCountryAndTerm }, dispatch);
}

function SearchPage({ fetchTopNewsByCountryAndTerm }) {
  const { t } = useTranslation();
  const { getRegion } = RegionContext();
  const [getLoader, setLoader] = useState(false);
  const [getTerm, setTerm] = useState("");
  const [getArticles, setArticles] = useState([]);

  const handleChange = (search) => {
    setTerm(search);
    setLoader(true);
    fetchTopNewsByCountryAndTerm(getRegion, search).then((res) => {
      if (res.payload && res.payload.data) {
        setLoader(false);
        setArticles(res.payload.data.articles);
      }
    });
  };

  const renderNews = () => {
    return getArticles.map((article, index) => {
      return <NewsCard key={"_" + index} article={article} />;
    });
  };

  useEffect(() => {
    fetchTopNewsByCountryAndTerm(getRegion, "").then((res) => {
      if (res.payload && res.payload.data) {
        setLoader(false);
        setArticles(res.payload.data.articles);
      }
    });
  }, [getRegion]);

  return (
    <SearchPageContainer>
      <SearchPageTitle>{t("pages.search") + getRegion}</SearchPageTitle>
      <CustomTextfield
        value={getTerm}
        label="Search"
        variant="outlined"
        onChange={(e) => handleChange(e.target.value)}
      />
      {getLoader && <CustomLoaderPartial />}
      <SearchPageContent>{getArticles ? renderNews() : "/"}</SearchPageContent>
    </SearchPageContainer>
  );
}

export default connect(null, mapDispatchToProps)(SearchPage);

SearchPage.propTypes = {
  fetchTopNewsByCountryAndTerm: PropTypes.func,
};
