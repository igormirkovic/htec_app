import React, { useState } from "react";
import { Redirect, useLocation } from "react-router-dom";
import styled from "styled-components";
import Button from "@material-ui/core/Button";
import { useTranslation } from "react-i18next";

const ArticleContainer = styled.div`
  width: 100%;
  height: 100%;
  text-align: center;
  padding: 30px 100px;
  box-sizing: border-box;
`;

const ArticleTitle = styled.span`
  font-size: 15px;
  font-weight: bolder;
`;

const ArticleImage = styled.img`
  width: 100%;
  margin-top: 40px;
  height: 370px;
  object-fit: contain;
`;

const ArticleContent = styled.div`
  width: 100%;
  text-align: justify;
  min-height: 150px;
  max-height: 300px;
  margin-top: 50px;
`;

const ArticleAction = styled.div`
  width: 100%;
  text-align: left;
  margin-top: 30px;
`;

export default function ArticlePage() {
  const location = useLocation();
  const { t } = useTranslation();
  const [getArticle] = useState(
    location && location.state && location.state.article
      ? location.state.article
      : {}
  );
  const [getRedirect, setRedirect] = useState("");

  if (getRedirect) {
    return (
      <Redirect
        push
        to={{
          pathname: getRedirect,
        }}
      />
    );
  }

  return (
    <ArticleContainer>
      <ArticleTitle>{getArticle.title}</ArticleTitle>
      <ArticleImage src={getArticle.urlToImage} />
      <ArticleContent>{getArticle.content}</ArticleContent>
      <ArticleAction>
        <Button onClick={() => setRedirect("/topnews")}>
          {t("article.toList")}
        </Button>
      </ArticleAction>
    </ArticleContainer>
  );
}
