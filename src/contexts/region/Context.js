import { createContext, useContext } from "react";

export const ContextDefinition = createContext({
  getRegion: "",
  setRegion: () => {},
});

export default () => useContext(ContextDefinition);
