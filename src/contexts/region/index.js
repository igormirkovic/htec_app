import React, { useState } from "react";
import RegionContext, { ContextDefinition } from "./Context";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";

const RegionState = (props) => {
  const [getRegion, setRegion] = useState("GB");
  const { i18n } = useTranslation();

  const setRegionAndLanguage = (lng) => {
    console.log(lng);
    i18n.changeLanguage(lng);
    setRegion(lng);
  };

  return (
    <ContextDefinition.Provider
      value={{
        getRegion,
        setRegion: setRegionAndLanguage,
      }}
    >
      {props.children}
    </ContextDefinition.Provider>
  );
};

export { RegionState, RegionContext };

RegionState.propTypes = {
  children: PropTypes.object,
};
