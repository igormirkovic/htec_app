import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import "./i18n";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import ReduxPromise from "redux-promise";

import reducers from "./reducers";
import { RegionState } from "./contexts/region";
import App from "./components/App.js";
import CustomLoader from "./components/controls/CustomLoader";

const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <BrowserRouter>
      <Suspense fallback={<CustomLoader />}>
        <RegionState>
          <App />
        </RegionState>
      </Suspense>
    </BrowserRouter>
  </Provider>,
  document.querySelector("#root")
);
