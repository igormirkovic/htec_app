import axios from "axios";
import {
  FETCH_TOPNEWS,
  FETCH_TOPNEWS_CATEGORIES,
  FETCH_TOPNEWS_SEARCH,
} from "./actionTypes";

const API_KEY = "4cac5c42831a41b0890c6891dd7f6f46";
const BASE_URL = `https://newsapi.org/v2/top-headlines?apiKey=${API_KEY}`;

export function fetchTopNewsByCountry(country) {
  const url = `${BASE_URL}&country=${country}`;
  const request = axios.get(url);
  return {
    type: FETCH_TOPNEWS,
    payload: request,
  };
}

export function fetchTopNewsByCountryAndCategories(country, category) {
  const url = `${BASE_URL}&country=${country}&category=${category}`;
  const request = axios.get(url);
  return {
    type: FETCH_TOPNEWS_CATEGORIES,
    payload: request,
  };
}

export function fetchTopNewsByCountryAndTerm(country, term) {
  const url = `${BASE_URL}&country=${country}&q=${term}`;
  const request = axios.get(url);
  return {
    type: FETCH_TOPNEWS_SEARCH,
    payload: request,
  };
}
